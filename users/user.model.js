const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const profile = new Schema({
    type: { type: String, required: true },
    headling: { type: String },
    description: { type: String }
});
const profileSchema = mongoose.model('ProfileData', profile);


const userSchema = new Schema({
    username: { type: String, unique: true, required: true },
    hash: { type: String, required: true },
    firstname: { type: String, required: true },
    lastname: { type: String, required: true },
    displayname: {type: String, required: true },
    createdDate: { type: Date, default: Date.now },
    profileData: {type: Schema.ObjectId, ref: 'ProfileData'}
});

userSchema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('User', userSchema);