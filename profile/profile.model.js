const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schemaProfile = new Schema({
    username: { type: String, unique: true, required: true },
    displayName: { type: String, unique: true, required: true },
    description: { type: String, required: false },
    imagePaths: { type: String, required: false },
    createdDate: { type: Date, default: Date.now }
});

schemaProfile.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Profile', schemaProfile);