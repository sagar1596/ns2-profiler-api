const express = require('express');
const router = express.Router();
const profileService = require('./profile.service');

// routes
router.post('/create', saveProfile);

module.exports = router;

function saveProfile(req, res, next) {
    profileService.saveProfileData(req.body)
        .then(profile => res.json(profile))
        .catch(err => next(err));
}