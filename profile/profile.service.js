const config = require('config.json');
const db = require('_helpers/db');
const Profile = db.Profile;

module.exports = {
    saveProfileData
}

async function saveProfileData(profileParam) {
    const profile = new Profile(profileParam);
    await profile.save();
}